# Algorithme des K-plus proches voisins

## Rappels : manipulation de données en tables

* Imports de _csv_
* Pré-traitement
    * _Pandas_
    * Normalisation des données avec la bibliothèque _statistic_

## Les distances
* Distance euclidienne
* Distance de Manhattan
* Distance de Hamming

==> Qui est le plus proche ?

## Quels sont les plus proches ? KNN
* Calcul des distances
* Tri des distances
* Détermination de la classe

==> A quelle classe appartient le point ?

## Approfondissement :
* _sklearn_

