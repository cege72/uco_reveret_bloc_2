def csv_vers_dict(adresse, separateur, encod = "utf-8") :
    """
    Crée une liste de dictionnaires à partir d'un fichier csv
    adresse est l'adresse du fichier sur le disque (string)
    separateur est le séparateur utilisé dans le fichier csv (string)
    encod est l'encodage du fichier (utf-8 par défaut)
    Renvoie une liste de dictionnaires
    """
    # Le tableau qui contiendra les dictionnaires (un par ligne)
    tableau = []
    
    # On ouvre le fichier
    with open(adresse, "r", encoding = encod) as fichier :
        # La première ligne contient les noms des clés
        cles = fichier.readline()[:-1].split(separateur)
        
        # On parcourt les lignes suivantes et on lit les couples de clé/valeur
        for ligne in fichier :
            valeurs = ligne[:-1].split(separateur)
            dico = dict()
            for cle, valeur in zip(cles, valeurs) :
                dico[cle] = valeur
            tableau.append(dico)
    
    return tableau


def csv_vers_liste(adresse, separateur, entetes, encod = "utf-8") :
    """
    Fonction important le fichier csv désigné par son adresse
    Le séparateur est fourni en argument
    Il est aussi possible de préciser l'encodage (utf-8 par défaut)
    et la présence d'entêtes dans la première ligne (celle-ci sera supprimée
    si la valeur passée est True)
    adresse est l'adresse du fichier sur le disque (string)
    separateur est le séparateur utilisé dans le fichier (string)
    entetes est un booléen indiquant si la première ligne du fichier 
    contient les entêtes des colonnes. Cette ligne sera supprimée si la 
    variable vaut True
    encod est l'encodage du fichier (utf-8 par défaut)
    Renvoie la liste de lignes du tableau
    """
    tableau = []

    with open(adresse, "r", encoding = encod) as fichier :
        for ligne in fichier :
            tableau.append(ligne[:-1].split(separateur))

    if entetes :
        tableau.pop(0)
        
    return tableau



def moyenne(tableau, colonne) :
    """
    Fonction calculant la moyenne de la colonne indiquée du tableau fourni en argument
    tableau est un tableau de tableaux ou de dictionnaires. Chaque ligne contient différentes colonnes
    colonne est le numéro ou la clé de la colonne dont on veut calculer la moyenne
    Renvoie la moyenne de la colonne concernée au format float
    """
    # Préconditions (partielles)\n
    assert isinstance(tableau, list), "tableau doit être une liste"
    assert type(colonne) in [int, str], "colonne doit être un entier ou une chaîne"

    somme = 0
    for ligne in tableau :
        somme += ligne[colonne]
    return somme / len(tableau)


def cherche(tableau, condition) :
    """
    Fonction cherchant dans le tableau donné, les lignes vérifiant la condition donnée
    tableau ets un tableau de tableau
    condition est une proposition du type "'man' in ligne[1] and ligne[-3] > 100" pour les héros 
    dont le nom contient 'man' et étant apparus plus de 100 fois
    Renvoie la liste des lignes trouvées ainsi que le nombre de lignes
    """
    
    resultat = [ligne for ligne in tableau if eval(condition)] 
    
    return resultat, len(resultat)
		
	
def jointure(table_A, cle_A, table_B, cle_B) :
    """
    Fonction effectuant la jointure des tables fournies en argument en comparant les cle_A et cle_B
    table_A et table_B sont des listes de dictionnaires
    cle_A et cle_B sont des clés de dictionnaires
    Renvoie la liste des dictionnaires pour lesquel les clés sont identiques
    """
    from copy import deepcopy
    
    table_resultat = []
    
    for ligne_A in table_A :
        for ligne_B in table_B :
            if ligne_A[cle_A] == ligne_B[cle_B] :
                dico = deepcopy(ligne_A)
                for cle in ligne_B :
                    if cle != cle_B :
                        dico[cle] = ligne_B[cle]
                table_resultat.append(dico)

    return table_resultat

	
def maxi(table, cle) :
    """
    Renvoie la ligne maximale d'une table en étudiant la valeur passée en clé
    table est une liste de liste ou de dictionnaires
    cle est une clé (str) ou un indice (int)
    Renvoie la ligne contenant le maximum et son indice (tuple)
    """
    def renvoie_valeur(l) :
        return l[cle]
    
    maximum = max(table, key = renvoie_valeur)
    indice_max = table.index(maximum)
    
    return maximum, indice_max
	
def mini(table, cle) :
    """
    Renvoie la ligne minimale d'une table en étudiant la valeur passée en clé
    table est une liste de liste ou de dictionnaires
    cle est une clé (str) ou un indice (int)
    Renvoie la ligne contenant le minimum et son indice (tuple)
    """
    def renvoie_valeur(l) :
        return l[cle]
    
    minimum = min(table, key = renvoie_valeur)
    indice_mini = table.index(minimum)
    
    return minimum, indice_mini
