# Tri et Correction

## Algorithme d'Euclide

## Correction des algorithmes

* Terminaison

    * Structure impérative
    * Boucle bornée ==> Variant de boucle
    * Boucle non bornée

* Correction partielle

    * Préconditions / Postconditions
    * Logique de Hoare
    * Démonstration dans le cas d'une structure impérative
    * Boucle et Invariant

## Correction des algorithmes de tri 

* Tri par sélection

* Tri par insertion

## Exercices

